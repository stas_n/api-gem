require 'active_record'
require 'pg'
class MarketingOptIn < ActiveRecord::Base


  enum permission_type: {one_time: 0, permanent: 1} unless instance_methods.include?(:permission_type)
  enum channel: {sms: 0, email: 1, sms_email: 2} unless instance_methods.include?(:channel)

  validates :company_name, uniqueness: {scope: :channel, message: "one channel per company"}
  validates :email, :mobile, :first_name, :last_name, :channel, :permission_type, :company_name, presence: true


  ActiveRecord::Base.establish_connection({adapter: 'postgresql', database: 'marketing'})


end