require_relative '../opt_in'
require 'spec_helper.rb'

describe Marketing::OptIn, :type => :api do
  it 'should display all records' do
    stub_request(:get, "http://localhost:9292/api/v1/marketings_opt_in").
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body =>
                                    [{"id" => 65, "email" => "1", "mobile" => "1", "first_name" => "1", "last_name" => "1",
                                      "channel" => "sms", "permission_type" => "permanent", "company_name" => "1"}], :headers => {})
  end


  it 'should create new record' do
    stub_request(:post, "http://localhost:9292/api/v1/marketings_opt_in?channel=sms&company_name=test&email=1&first_name=test&id=99999&last_name=1&mobile=1&permission_type=permanent").
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})
  end

  it 'should find record by id' do
    stub_request(:get, "http://localhost:9292/api/v1/marketings_opt_in/99999").
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})
  end

  it 'should update record by id and specific params' do
    stub_request(:put, "http://localhost:9292/api/v1/marketings_opt_in/99999?last_name=test").
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})
  end


  it 'should destroy record' do
    stub_request(:delete, "http://localhost:9292/api/v1/marketings_opt_in/99999").
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})
  end
end