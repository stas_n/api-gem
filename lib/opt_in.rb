module Marketing
  class OptIn
    require 'httparty'

    API_URL = 'http://localhost:9292/api/v1/marketings_opt_in'

    class << self

      def all
        response = HTTParty.get(API_URL)
        JSON.parse(response.body)
      end

      def find(id)
        response = HTTParty.get("#{API_URL}/#{id}")
        JSON.parse(response.body)
      end

      def create(params)
        http_params = params.to_query
        response = HTTParty.post("#{API_URL}?#{http_params}")
        response.code
      end

      def update(id,params)
        http_params = params.to_query
        response = HTTParty.put("#{API_URL}/#{id}?#{http_params}")
        JSON.parse(response.body)
      end

      def destroy(id)
        response = HTTParty.delete("#{API_URL}/#{id}")
        response.code
      end

    end

  end
end