Gem::Specification.new do |s|
  s.name        = 'opt_in'
  s.version     = '0.0.1.2'
  s.date        = '2016-03-09'
  s.summary     = 'summary!'
  s.description = 'Fetch data from server on my server'
  s.authors     = ['Stanislav Nosovskyi']
  s.email       = 'my_email'
  s.files       = ['lib/opt_in.rb']
  s.homepage    =
      'http://rubygems.org/gems/opt_in'
  s.license       = 'MIT'
  s.add_runtime_dependency 'httparty',
                           ['~>0.13.7']


end