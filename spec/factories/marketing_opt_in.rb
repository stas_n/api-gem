FactoryGirl.define do
  factory :marketing_opt_in do
    email 'email@mail.com'
    mobile 381234567890
    first_name 'Stas'
    last_name 'Nosovskyi'
    channel 'sms'
    permission_type 'one_time'
    company_name ('a'..'z').to_a.sample(5).join()
  end
end
