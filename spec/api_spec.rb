require './spec/spec_helper.rb'

describe 'Api' do
  before(:all) do
    @marketing = create(:marketing_opt_in)
  end

  it 'should display all records' do
    get '/api/v1/marketings_opt_in'
    expect(last_response).to be_ok
    expect(last_response.status).to eq(200)
  end

  it 'should display one record' do
    get "/api/v1/marketings_opt_in/#{@marketing.id}"
     expect(last_response).to be_ok
    expect(last_response.status).to eq(200)
  end

  it 'should create record' do
    post "/api/v1/marketings_opt_in",first_name:11,channel:'sms'
    expect(last_request.params).to eq({"first_name"=>"11", "channel"=>"sms"})
  end


  it 'should update record' do
    put "/api/v1/marketings_opt_in/#{@marketing.id}?first_name=22"
    @marketing.reload
    expect(@marketing.first_name).to eq('22')

  end

  it 'should delete record' do
    delete "/api/v1/marketings_opt_in/#{@marketing.id}"
    expect(last_response.status).to eq 202
  end
end
