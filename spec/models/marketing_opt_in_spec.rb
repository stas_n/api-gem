require './app/models/marketing_opt_in'
require './spec/spec_helper.rb'

describe MarketingOptIn do
  it "should be uniq" do
    m1 = MarketingOptIn.create(company_name:1,channel:'sms',email:1,mobile:1,last_name:1,first_name:1,permission_type:'permanent')
    expect(m1).to be_valid
    m2 = MarketingOptIn.create(company_name:1,channel:'sms')
    expect(m2).not_to be_valid
  end
end