require 'rack/test'
require 'rspec'
require 'database_cleaner'
require 'webmock/rspec'
require 'factory_girl'



ENV['RACK_ENV'] = 'test'

require './api'
require './api.rb'
require 'factories/marketing_opt_in'


module RSpecMixin
  include Rack::Test::Methods

  def app()
    Api.new
  end
end

RSpec.configure do |config|
  config.include RSpecMixin
  config.before(:all) do
    WebMock.disable_net_connect!
  end
  config.include FactoryGirl::Syntax::Methods

  config.before do
    DatabaseCleaner.start
  end

  config.after do
    DatabaseCleaner.clean
  end
end

