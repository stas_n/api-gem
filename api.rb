require 'sinatra/base'
class Api < Sinatra::Base
  require 'sinatra'
  require 'JSON'
  load './app/models/marketing_opt_in.rb'

# list all
  get '/api/v1/marketings_opt_in' do
    MarketingOptIn.all.to_json
    # status 200
  end

# view one
  get '/api/v1/marketings_opt_in/:id' do
    marketing_opt_in = MarketingOptIn.find(params[:id])
    return status 404 if marketing_opt_in.nil?
    marketing_opt_in.to_json
  end

# create
  post '/api/v1/marketings_opt_in' do
    marketing_opt_in = MarketingOptIn.new(request.params)
    if marketing_opt_in.save
      status 200
    else
      {
          errors: marketing_opt_in.errors,
          status: 409
      }.to_json

    end
  end

# update
  put '/api/v1/marketings_opt_in/:id' do
    marketing_opt_in = MarketingOptIn.find(params[:id])
    return status 404 if marketing_opt_in.nil?
    marketing_opt_in.update(request.params)
    marketing_opt_in.save
    {
        marketing_opt_in: marketing_opt_in,
        status: 202
    }.to_json
  end

  delete '/api/v1/marketings_opt_in/:id' do
    marketing_opt_in = MarketingOptIn.find(params[:id])
    return status 404 if marketing_opt_in.nil?
    marketing_opt_in.delete
    status 202
  end
end